﻿using Pokedex.Models;
using Pokedex.Services;
using Pokedex.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pokedex
{
    public partial class MainPage : ContentPage
    {
        



        public MainPage()
        {
            //BindingContext = this;
            BindingContext = new MainViewModel();
            
            InitializeComponent();
           
        }

        private void OnBtnNextClicked(object sender, EventArgs e)
        {
            ((Button)sender).BackgroundColor = Color.Red;
            Btn_1.BackgroundColor = Color.Green;
        }
    }
}
