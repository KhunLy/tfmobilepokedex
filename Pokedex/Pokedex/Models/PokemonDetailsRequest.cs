﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Models
{
    public class PokemonDetailsRequest
    {
        public string Name { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public Sprites Sprites { get; set; }
    }
}
