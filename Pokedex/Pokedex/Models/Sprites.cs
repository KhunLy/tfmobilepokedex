﻿using Newtonsoft.Json;

namespace Pokedex.Models
{
    public class Sprites
    {
        [JsonProperty("front_default")]
        public string FrontDefault { get; set; }

        [JsonProperty("back_default")]
        public string BackDefault { get; set; }
    }
}