﻿using Pokedex.Models;
using Pokedex.Services;
using Pokedex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pokedex
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PokemonDetailsModalPage : ContentPage
	{

        public PokemonDetailsModalPage (string url)
		{
            BindingContext = new PokemonDetailsModalViewModel(url);
			InitializeComponent();
		}
    }
}