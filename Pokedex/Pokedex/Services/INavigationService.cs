﻿namespace Pokedex.Services
{
    public interface INavigationService
    {
        void CloseModal();
        void OpenPokemonDetails(string url);
    }
}