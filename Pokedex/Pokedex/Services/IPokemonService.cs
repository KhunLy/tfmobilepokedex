﻿using System.Threading.Tasks;
using Pokedex.Models;

namespace Pokedex.Services
{
    public interface IPokemonService
    {
        Task<PokemonsRequest> GetAllAsync(string url);
        Task<PokemonDetailsRequest> GetAsync(string url);
    }
}