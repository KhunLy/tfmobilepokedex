﻿using Pokedex.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Pokedex.Services
{
    public class NavigationService : INavigationService
    {
        //public NavigationService()
        //{
        //    MessagingCenter.Subscribe<MainViewModel,string>
        //        (this, "OpenDetails", (sender, url) =>
        //    {
        //        App.Current.MainPage
        //                .Navigation.PushModalAsync(
        //                new PokemonDetailsModalPage(url)
        //            );
        //    });
        //    MessagingCenter.Subscribe<PokemonDetailsModalViewModel>
        //        (this, "CloseModal", (sender) => {
        //        App.Current.MainPage.Navigation.PopModalAsync();
        //    });
        //}

        public void CloseModal()
        {
            App.Current.MainPage.Navigation.PopModalAsync();
        }

        public void OpenPokemonDetails(string url)
        {
            App.Current.MainPage.Navigation.PushModalAsync(
                new PokemonDetailsModalPage(url)    
            );
        }
    }
}
