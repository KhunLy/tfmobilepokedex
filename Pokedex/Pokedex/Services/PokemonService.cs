﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pokedex.Models;

namespace Pokedex.Services
{
    public class PokemonService : IPokemonService
    {
        public async Task<PokemonsRequest> GetAllAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage message
                = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    string json
                        = await message.Content
                        .ReadAsStringAsync();
                    return JsonConvert
                        .DeserializeObject<PokemonsRequest>(json);
                }
                return null;
            }
        }

        public async Task<PokemonDetailsRequest> GetAsync(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage message
                = await client.GetAsync(url);
                if (message.IsSuccessStatusCode)
                {
                    string json
                        = await message.Content
                        .ReadAsStringAsync();
                    return JsonConvert
                        .DeserializeObject<PokemonDetailsRequest>(json);
                }
                return null;
            }
        }
    }
}
