﻿using Pokedex.Models;
using Pokedex.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pokedex.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        private ObservableCollection<Pokemon> _ListePokemon;

        public ObservableCollection<Pokemon> ListePokemon
        {
            get { return _ListePokemon; }
            set { _ListePokemon = value; }
        }

        private string _Next;

        private string _Previous;

        private bool CanGoPrev()
        {
            return _Previous != null && !IsBusy; 
        }

        private bool CanGoNext()
        {
            return _Next != null && !IsBusy; 
        }

        private Pokemon _SelectedPokemon;

        public Pokemon SelectedPokemon
        {
            get { return _SelectedPokemon; }
            set
            {
                if (_SelectedPokemon != value)
                {
                    _SelectedPokemon = value;
                    DependencyService.Get<INavigationService>()
                        .OpenPokemonDetails(value.Url);
                }
                _SelectedPokemon = null;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            ListePokemon =
                new ObservableCollection<Pokemon>();
            LoadItems();
        }
        private Command _GoPrevCmd;
        public Command GoPrevCmd
        {
            get
            {
                return
                _GoPrevCmd = _GoPrevCmd ?? new Command(GoPrev, CanGoPrev);
            }
        }
        private Command _GoNextCmd;
        public Command GoNextCmd
        {
            get {
                return _GoNextCmd = _GoNextCmd ?? 
                    new Command(GoNext, CanGoNext);
            }
        }

        private void LoadItems()
        {
            RefreshItems("https://pokeapi.co/api/v2/pokemon");
        }

        private void GoPrev()
        {
            if (_Previous != null)
            {
                RefreshItems(_Previous);
            }
        }

        private void GoNext()
        {
            if (_Next != null)
            {
                RefreshItems(_Next);
            }
        }

        private async void RefreshItems(string url)
        {
            IsBusy = true;
            GoPrevCmd.ChangeCanExecute();
            GoNextCmd.ChangeCanExecute();
            IPokemonService PkmonService =
                DependencyService.Get<IPokemonService>();
            PokemonsRequest req =
                await PkmonService
                .GetAllAsync(url);
            ListePokemon.Clear();
            foreach (Pokemon p in req.Results)
            {
                ListePokemon.Add(p);
            }
            _Next = req.Next;
            _Previous = req.Previous;
            IsBusy = false;

            GoPrevCmd.ChangeCanExecute();
            GoNextCmd.ChangeCanExecute();
        }
    }
}
