﻿using Pokedex.Models;
using Pokedex.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Pokedex.ViewModels
{
    public class PokemonDetailsModalViewModel
        : ViewModelBase
    {
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetValue(ref _Name, value); }
        }

        private double _Height;

        public double Height
        {
            get { return _Height; }
            set { SetValue(ref _Height, value); }
        }

        private int _Weight;

        public int Weight
        {
            get { return _Weight; }
            set { SetValue(ref _Weight, value); }
        }


        private string _FrontDefault;

        public string FrontDefault
        {
            get { return _FrontDefault; }
            set { SetValue(ref _FrontDefault, value); }
        }


        private string _BackDefault;

        public string BackDefault
        {
            get { return _BackDefault; }
            set { SetValue(ref _BackDefault, value); }
        }

        public PokemonDetailsModalViewModel(string url)
        {
            LoadItems(url);
        }

        public Command NavigateToIndexCmd
        {
            get
            {
                return new Command(NavigateToIndex);
            }
        }

        private void NavigateToIndex()
        {
            DependencyService.Get<INavigationService>()
                .CloseModal();
        }

        private async void LoadItems(string url)
        {
            PokemonDetailsRequest pkmn = await DependencyService.Get<IPokemonService>()
                .GetAsync(url);
            Name = pkmn.Name;
            Height = pkmn.Height / 10d;
            Weight = pkmn.Weight;
            FrontDefault = pkmn.Sprites.FrontDefault;
            BackDefault = pkmn.Sprites.BackDefault;
        }
    }
}
