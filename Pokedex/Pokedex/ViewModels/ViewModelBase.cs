﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Pokedex.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _IsBusy;

        public bool IsBusy
        {
            get { return _IsBusy; }
            set { SetValue(ref _IsBusy, value); }
        }


        protected virtual void OnPropertyChanged
            ([CallerMemberName]string propName = "")
        {
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(propName)
            );
        }

        protected void SetValue<T>
            (ref T oldProp, 
            T newValue, 
            [CallerMemberName] string propName = "")
        {
            if(!EqualityComparer<T>.Default.Equals(oldProp, newValue))
            {
                oldProp = newValue;
                OnPropertyChanged(propName);
            }
        }
    }
}
